String use lots of memory. In some production applications, they use 25-40 percent of the memory in the entire program.

The string pool or intern pool, is a location in the JVM that collects commonly used strings.

Only literals like `"name"` gets stored in the string pool. `myobj.toString()` is a string, but not a literal, so does not go into string pool. 

Strings not in String pool are garbage collected.

``` java linenums="1 1 2"
    String name = "Fluffy";
    String name = new String("Fluffy");
```

The first line uses string pool normally.

The second line creates a string object.