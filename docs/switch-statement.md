Switch statement is a complex decision-making structure. 

``` java linenums="1 1 2"
    switch(var){
        case constantExpression1:
        Branch for case1;
        break

        case constantExpression2:
        Branch for case2;
        break

        default:
        Branch for default
    }
```

Supported data types:

  * int and Integer
  * byte and Byte
  * short and Short
  * char and Character
  * int and Integer
  * String
  * enum values

Case statements must use constant variables at runtime.