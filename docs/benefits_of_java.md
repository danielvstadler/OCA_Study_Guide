|||
|--|:--|
|OOP|Code is defined in classes and most of those classes can be instansiated into objects. |
|Encapsulation|Access modifiers are used to protect data from unintended access or modification. Encapsulation is a part of OOP, but exam pays special attention to it.|
|Platform Independant|Java can be compiled once rather than needing to be recompiled for different operating systems.|
|Robust|Prevents memory leaks.|
|Simple|Java is simpler than C++. Removes pointers and operator overloading.|
|Secure|Java code runs inside JVM. this creates a sandbox that makes it hard for java code to do anything evil to pc it is running on.|