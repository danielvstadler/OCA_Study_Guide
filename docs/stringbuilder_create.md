three ways to construct a StringBuilder:

``` java linenums="1 1 2"
    StringBuilder sb1 = new StringBuilder();//empty sequence of chars
    StringBuilder sb2 = new StringBuilder("animal");//spesific value
    StringBuilder sb3 = new StringBuilder(10);//reserve 10 char spaces
```

???+ note "Size vs. Capacity"

    !!! note "not in OCA"

    size is number of characters.

    capacity is number of characters that can currently be held.

    In a String object size=capacity.

    in a StringBuilder Java increases capacity as you use object.