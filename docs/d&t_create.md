Dates and Times use `import java.time.*`

Choices of information:

  * __LocalDate__ Contains just the date.
  * __LocalTime__ Contains just the time.
  * __LocalDateTime__ Contains both date and time.

``` java linenums="1 1 2"
    System.out.println(LocalDate.now());//2015-01-20
    System.out.println(LocalTime.now());//12:45:18.401
    System.out.println(LocalDateTime.now());//2015-01-20T12:45:18.401
```

Date:

``` java linenums="1 1 2"
    LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
    LocalDate date2 = LocalDate.of(2015, 1, 20);
```

Time:

``` java linenums="1 1 2"
    LocalTime time1 = LocalTime.of(6, 15); // hour and minute
    LocalTime time2 = LocalTime.of(6, 15, 30); // + seconds
    LocalTime time3 = LocalTime.of(6, 15, 30, 200); // + nanoseconds
```

Combine Date and Time:

``` java linenums="1 1 2"
    LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30);
    LocalDateTime dateTime2 = LocalDateTime.of(date1, time1);
```
