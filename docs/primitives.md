|Keyword|Type|
|:------- |:--------------- |
| Boolean | True or False   |
| Byte    | 8-bit intergral |
| Short   | 16-bit intergral|
| Int     | 32-bit intergral|
| Long    | 64-bit intergral|
| Float   | 32-bit floating-point value|
| Double  | 32-bit floating-point value|
| Char    | 16-bit Unicode value|

!!! info "Primitives cannot be set to null"

Float requires the letter f following the number. 

Long is L at following number.

Byte, short, int and long are used for numbers without decimals.

Each numeric type uses twice as many bits as the smaller similar type. Ex: short uses twice as many bits as byte does.

Int max size is 2,147,483,647.
