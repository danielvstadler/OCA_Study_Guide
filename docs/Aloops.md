##Nested Loops

``` java linenums="1 1 2"
    int[][] myComplexArray = {{5,2,1,3},{3,9,8,9},{5,7,12,7}};
    for(int[] mySimpleArray : myComplexArray) {
        for(int i=0; i<mySimpleArray.length; i++) {
            System.out.print(mySimpleArray[i]+"\t");
        }
        System.out.println();
    }//Runs 3 times
```

``` java linenums="1 1 2"
    while(x>0) {
        do {
            x -= 2
        } while (x>5);
        x--;
        System.out.print(x+"\t");
    }
```

Result of above example is: 3  0


##Optional Labels

``` java linenums="1 1 2"
    Outer_loop: for(int i=0;arr.length;i++>){
        Inner_loop: for(int i=0;arr.length;i++>){
        
        }
    }
```

##break and continue

break statements transfers the flow of control out of enclosing statement.

``` java linenums="1 1 2"
    break Outer_loop;//Can exit spesific loops based on optional labels
    break;//Exit current loop
```

continue statement ends current iteration of the loop.

``` java linenums="1 1 2"
    continue Outer_loop;//Can continue spesific loops based on optional labels
    continue;//continue current loop
```

||Optional labels|Allow break|Allow continue|
|--|:--:|:--:|:--:|
|if|Yes|No|No|
|while|Yes|Yes|Yes|
|do while|Yes|Yes|Yes|
|for|Yes|Yes|Yes|
|switch|Yes|Yes|No|