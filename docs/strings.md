String is a reference type.

``` java linenums="1 1 2"
    String name = "Fluffy";
    String name = new String("Fluffy");
```

Both give you a reference variable of type name pointing to the String object "Fluffy".
