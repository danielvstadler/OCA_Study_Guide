Refers to an object.

Primitives hold their value in in memory where the var is allocated, references point to the address in memory where the object is located.

!!! info "Reference types can be set to null."
