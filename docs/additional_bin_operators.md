##Assignment Operators

An assignment operator modifies or assigns variable on left-hand. 

Java automatically converts from smaller to larger data type, but throws exception if you try to set larger data type to smaller type.

###Casting Primitive Values:
Casting is required if you are going from a large data type to a smaller one.

???+ interesting "Overflow and Underflow"
    if casting from bigger value to smaller value and value is too high, Overflow occurs.

    if value is too low Underflow occurs.

``` java linenums="1 1 2"
        short x = 10;
        short y = 3;
        short z = x * y;
        //Does not COMPILE
        //Reason: 
        //data types smaller than int get 
        //promoted to int, but int can't be set to smaller short.

        //Fix:
        short z = (short)(x*y);
```

##Compound Assignment Operators:

|Examples|
|--|
|+=|
|-=|
|*=|

Compound Assignment Operators also cast values without the need of explicitly typing them.

##Relational Operators:

|Symbol|Description|
|--|--|
|<|Strictly less than|
|<=|Less than or equal to|
|>|> Strictly greater than|
|>=|Greater than or equal to|

##Logical Operators:

AND(&)

||||
|-|-|-|
|   |y = true|y = false|
|x = true|true|false|
|x = false|false|false|

Inclusive OR(|)

||||
|-|-|-|
|   |y = true|y = false|
|x = true|true|true|
|x = false|true|false|

Exclusive OR(^)

||||
|-|-|-|
|   |y = true|y = false|
|x = true|false|true|
|x = false|true|false|

##Equality Operators:

equality operator are used in one of three scenarios:

  * Comparing numeric types. data types automatically promoted.
  * Comparing boolean types.
  * Comparing two objects, including String and null.

Cannot compare different data types.

For object comparison, the equality operator applies to references to the objects, not to the objects they point to.

Two references are equal if they point to the same object.

Example:

``` java linenums="1 1 2"
    File x = new File("myFile.txt");
    File y = new File("myFile.txt");
    File z = x;
    System.out.println(x == y); // Outputs false
    System.out.println(x == z); // Outputs true

```

##Ternary Operator

ternary operator or conditional operator, is the only operator that takes 3 operands and is in form:

`boolean expression ? expression1 : expression2`

Example:

``` java linenums="1 1 2"
    int y = 10;
    final int x;
    if(y > 5) {
        x = 2 * y;
    } else {
        x = 3 * y;
    }
    //convert to ternary operator
    int y = 10;
    int x = (y>5)?(2*y):(3*y)
```

