ArrayList just like StringBuilder its size can change at runtime.

ArrayList is an ordered sequence taht allows duplicates.

imports are required:

``` java linenums="1 1 2"
    import java.util.* // import whole package including ArrayList
    import java.util.ArrayList; // import just ArrayList
```

In OCA Exam you can assume imports are done.