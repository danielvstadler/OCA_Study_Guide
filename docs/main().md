Java program begins execution with its main() method. 
Main() is the gateway between the start-up of a java process, which is managed by Java Virtual Machine(JVM), and the beginning of the programs code.

The JVM calls underlying system to allocate memory and cpu time, access file, etc.
The main() method hooks programmer code into this process, keeping it alive long enough to do the work required.

Example:

``` java linenums="1 1 2"
    public class Zoo {
        public static void main(String[] args) {
        }
    }
```

!!! info "The name of the file must match the name of the class."

