###length()

Returns number of characters in string.

``` java linenums="1 1 2"
    string.length()
``` 

###charAt()

Returns char at spesific index.

``` java linenums="1 1 2"
    string.charAt(0);
``` 

If argument value exceeds number of characters in string, exception is thrown.

###indexOf()

Method takes in character or string, and optionally index to start at. Then return index of first occurence in String.

``` java linenums="1 1 2"
    String string = "animals";
    System.out.println(string.indexOf('a')); // 0
    System.out.println(string.indexOf("al")); // 4
    System.out.println(string.indexOf('a', 4)); // 4
    System.out.println(string.indexOf("al", 5)); // -1
``` 

!!! note "In Line 5 indexOf can't find occurence after index provided, but does not throw exception, instead just returns -1."

###substring()

Returns substring of string between two arguments passed in. 

!!! danger "Does not include character at latter index."

!!! danger "Latter index cannot be smaller than first."

!!! note "Latter index can exceed string length by 1."

``` java linenums="1 1 2"
    String string = "animals";
    System.out.println(string.substring(3)); // mals
    System.out.println(string.substring(string.indexOf('m'))); // mals
    System.out.println(string.substring(3, 4)); // m
    System.out.println(string.substring(3, 7)); // mals
```

###toLowerCase() and toUpperCase()

returns lowercase or uppercase of string. Leaves numbers and already correct letters alone.

``` java linenums="1 1 2"
    string.toLowerCase();
    string.toUpperCase();
```

###equals and equalsIgnoreCase()

Check if two strings contain the same characters. 

equalsIgnoreCase() ignores case of the string.

``` java linenums="1 1 2"
    System.out.println("abc".equals("ABC")); // false
    System.out.println("ABC".equals("ABC")); // true
    System.out.println("abc".equalsIgnoreCase("ABC")); // true
```

###startsWith() and endsWith()

``` java linenums="1 1 2"
    System.out.println("abc".startsWith("a")); // true
    System.out.println("abc".startsWith("A")); // false
    System.out.println("abc".endsWith("c")); // true
    System.out.println("abc".endsWith("a")); // false
```
Case sensitive.

###contains()

``` java linenums="1 1 2"
    System.out.println("abc".contains("b")); // true
    System.out.println("abc".contains("B")); // false
```

Case sensitive.

###replace()

Simple search and replace.

``` java linenums="1 1 2"
    System.out.println("abcabc".replace('a', 'A')); // AbcAbc
    System.out.println("abcabc".replace("a", "A")); // AbcAbc
```

###trim()

Removes white spaces in front and behind string.

``` java linenums="1 1 2"
    System.out.println("abc".trim()); // abc
    System.out.println("\t a b c\n".trim()); // a b c
```