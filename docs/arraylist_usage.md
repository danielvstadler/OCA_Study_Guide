ArrayList implements toString() meaning it prints pretty, unlike Array.

!!! warning "If you spesify class that ArrayList can contain, no other type can be added."

##add()

Inserts a new value into ArrayList.

``` java linenums="1 1 2"
    ArrayList list = new ArrayList();
    list.add("hawk"); // [hawk]
    list.add(Boolean.TRUE); // [hawk, true]
    System.out.println(list); // [hawk, true]
```

##remove()

Removes first matching value or element at provided index. 

Returns true or false if successfull or not.

``` java linenums="1 1 2"
    List<String> birds = new ArrayList<>();
    birds.add("hawk"); // [hawk]
    birds.add("hawk"); // [hawk, hawk]
    System.out.println(birds.remove("cardinal")); // prints false
    System.out.println(birds.remove("hawk")); // prints true
    System.out.println(birds.remove(0)); // prints hawk
    System.out.println(birds); // []
```

##set()

set() changes one element of ArrayList at a spesific index.

``` java linenums="1 1 2"
    List<String> birds = new ArrayList<>();
    birds.add("hawk"); // [hawk]
    System.out.println(birds.size()); // 1
    birds.set(0, "robin"); // [robin]
    System.out.println(birds.size()); // 1
    birds.set(1, "robin"); // IndexOutOfBoundsException
```

##isEmpty() and size()

``` java linenums="1 1 2"
    System.out.println(birds.isEmpty()); // true
    System.out.println(birds.size()); // 0
    birds.add("hawk"); // [hawk]
    birds.add("hawk"); // [hawk, hawk]
    c03.indd 1½ 4/2014 Page 133
    System.out.println(birds.isEmpty()); // false
    System.out.println(birds.size()); // 2
```

##clear()

Discard all elements in ArrayList.

``` java linenums="1 1 2"
    ArrayList.clear();
```

##contains()

Checks if value is in arrayList. returns true or false.

``` java linenums="1 1 2"
    List<String> birds = new ArrayList<>();
    birds.add("Hawk");
    birds.contains("Hawk");//true
    birds.contains("robin");//false
```

##equals()

ArrayList has a custom implementation that allows you to compare two ArrayList according to Elements and order.

``` java linenums="1 1 2"
    List<String> one = new ArrayList<>();
    List<String> two = new ArrayList<>();
    System.out.println(one.equals(two)); // true
    one.add("a"); // [a]
    System.out.println(one.equals(two)); // false
    two.add("a"); // [a]
    System.out.println(one.equals(two)); // true
    one.add("b"); // [a,b]
    two.add(0, "b"); // [b,a]
    System.out.println(one.equals(two)); // false
```
