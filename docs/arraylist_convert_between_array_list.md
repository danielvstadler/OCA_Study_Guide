###From ArrayList to Array

```java linenums="1 1 2"
    List<String> list = new ArrayList<>();
    list.add("hawk");
    list.add("robin");
    Object[] objectArray = list.toArray();
    System.out.println(objectArray.length); // 2
    String[] stringArray = list.toArray(new String[0]);
    System.out.println(stringArray.length); // 2
```

!!! note "toArray() defaults to an array of class Object. This can be changed like in line 6."

###From Array to List

Original array and array backed list are linked. What happens in one happens in other. Created backed list is fixed.

``` java linenums="1 1 2"
    String[] array = { "hawk", "robin" }; // [hawk, robin]
    List<String> list = Arrays.asList(array); // returns fixed size list
    System.out.println(list.size()); // 2
    list.set(1, "test"); // [hawk, test]
    array[0] = "new"; // [new, test]
    
    for (String b : array){ 
        System.out.print(b + " "); // new test
    }

    list.remove(1); // throws UnsupportedOperation Exception
```