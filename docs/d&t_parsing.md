``` java linenums="1 1 2"
    DateTimeFormatter f = DateTimeFormatter.ofPattern("MM dd yyyy");
    LocalDate date = LocalDate.parse("01 02 2015", f);//using custom
    LocalTime time = LocalTime.parse("11:22");//using default formating
    System.out.println(date); // 2015-01-02
    System.out.println(time); // 11:22
```

Exceptions can be thrown on invalid dates or times.