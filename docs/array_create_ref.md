Any Java object can be used to be type of array.

``` java linenums="1 1 2"
    public class ArrayType {
        public static void main(String args[]) {
            String [] bugs = { "cricket", "beetle", "ladybug" };
            String [] alias = bugs;
            System.out.println(bugs.equals(alias)); // true
            System.out.println(bugs.toString()); // [Ljava.lang.String;@160bc7c0
        } 
    }
```
equals() above checks returns true because of reference equality not because the content is the same.


!!! note "Since java 5 java
.util.Arrays.toString(bugs) would print nicely. Exam tends to not use it because questions were written a long time ago."

``` java linenums="1 1 2"
    String[] strings = { "stringValue" };
    Object[] objects = strings;
    String[] againStrings = (String[]) objects;
    againStrings[0] = new StringBuilder(); // DOES NOT COMPILE
    objects[0] = new StringBuilder(); // careful!
```

array of objects can be cast to array of strings.

Array of strings cannot convert a element to StringBuilder. A Array of Objects is be able to.