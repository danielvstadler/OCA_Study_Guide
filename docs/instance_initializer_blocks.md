Code blocks outside are called instance initializer blocks.

``` java linenums="1 1 2"
    public static void main(String[] args) { 
        { 
            System.out.println("Feathers"); 
        } 
    }
    { 
        System.out.println("Snowy"); //Example of iib, outside of method 
    }  

```