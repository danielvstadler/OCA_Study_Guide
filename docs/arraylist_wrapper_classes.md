Each primitive type has a wrapper class, which is an object type, that corresponds to that primitive.

|Primitive Types|Wrapper Classes|Example of constructing|
|:--|:--|:--|
|boolean|Boolean|new Boolean(true)|
|byte|Byte|new Byte((byte)1)|
|short|ByteShort|new Short((short)1)|
|int|Integer|new Integer(1)|
|long|Long|new Long(1)|
|float|Float|new Float(1.0)|
|double|Double|new Double(1.0)|
|char|Character|new Character('c')|

???+ note "Wrapper Functions"

    ``` java linenums="1 1 2"
        int primitive = Integer.parseInt("123");//Returns primitive
        Integer wrapper = Integer.valueOf("123");//Returns Integer Wrapper class

        int bad1 = Integer.parseInt("a"); // throws NumberFormatException
        Integer bad2 = Integer.valueOf("123.45"); // throws NumberFormatException
    ```

    |Wrapper Class|String to primitive|String to Wrapper|
    |--|--|--|
    |Boolean|Boolean.parseBoolean("true");|Boolean.valueOf("TRUE")|
    |Byte|Byte.parseByte("1");|Byte.valueOf("1")|
    |Short|Short.ParseShort("1")|Short.valueOf("1")|
    |Integer|Integer.parseInt("1")|Integer.valueOf("1")|
    |Long|Long.parseLong("1")|Long.valueOf("2")|
    |Float|Float.parseFloat("1")|Float.valueOf("2.2")|
    |Double|Double.parseDouble("1")|Double.valueOf("1.1")|
    |Character|none|none|