To create an instance of a class requires that you type new.
Example:

``` java linenums="1 1 2"
    R = new random()
```

Method inside class with the same name as class, Capital letter and no return type is constructor.


``` java linenums="1 1 2"
    public class Chicken { 
        int numEggs = 0;// initialize on line String name; 

        public Chicken() { 
            name = "Duke";// initialize in constructor 
        } 
    }

```