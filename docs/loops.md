##While Statement

Will continue as long as boolean condition evaluates to true.
Boolean expression is evaluated before each iteration. This means that it is possible for while loop to terminate before first iteration.


``` java linenums="1 1 2"
    while(booleanExpression){
        //Body
    }
```

!!! warning "Don't create infinite loops! loops need a way out."

##Do-while Statement

Do-While loops run at least once as condition is checked after iteration.

``` java linenums="1 1 2"
    do{
        //Body
    }while(booleanExpression);
```

##for Statement

``` java linenums="1 1 2"
    for(initialization;booleanExpression;updateStatement){
        //Body
    }
```

???+ note "Infinte for Loop"

    ``` java linenums="1 1 2"
        for(;;){
            //Body
        }
    ```

    This shows that for loops do not need all components.

???+ note "Multiple Terms"

    ``` java linenums="1 1 2"
        int x = 0;
        for(long y = 0, z = 4; x < 5 && y < 10; x++, y++) {
            System.out.print(y + " ");
        }
        System.out.print(x);
    ```    

    Interestingly x can be initialized outside for loop and incremented inside loop.

???+ note "Redeclaring a Variable in the Initialization block"

    ``` java linenums="1 1 2"
        int x = 0;
        for(long y = 0, x = 4; x < 5 && y < 10; x++, y++) { // DOES NOT COMPILE
            System.out.print(x + " ");
        }
    ```

    This example does not compile because, x is re-declared. 

    This is fixed by removing the `long` keyword in initialization.

!!! note "Variables in initialization must be the same type."

!!! danger "Variables declared in initialization block or inside loop are only scoped to that loop." 

##for-each Statement

``` java linenums="1 1 2"
    for(datatype instance : collection){

    }
```

!!! danger "In Exam make sure collection is array or list.(NOT Python cannot have string as collection)"