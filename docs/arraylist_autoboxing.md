Since Java 5 you can just type the primitive and Java converts it to the relevant Wrapper Class for you. This is called autoboxing.

``` java linenums="1 1 2"
    List<Double> weights = new ArrayList<>();
    weights.add(50.5); // [50.5]
    weights.add(new Double(60)); // [50.5, 60.0]
    weights.remove(50.5); // [60.0]
    double first = weights.get(0); // 60.0
```

In line 2 and 4, primitive is auto boxed into Double Object.

In line 3, Pass in wrapper class.

In line 5, object and unboxes it into a primitive.
