!!! important "Date and Time classes are immutable so remember to assign them to a variable."

``` java linenums="1 1 2"
    //Add time
    LocalDate date = LocalDate.of(2014, Month.JANUARY, 20);
    System.out.println(date); // 2014-01-20
    date = date.plusDays(2);
    System.out.println(date); // 2014-01-22
    date = date.plusWeeks(1);
    System.out.println(date); // 2014-01-29
    date = date.plusMonths(1);
    System.out.println(date); // 2014-02-28
    date = date.plusYears(5);
    System.out.println(date); // 2019-02-28

    //Subtract time
    LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
    LocalTime time = LocalTime.of(5, 15);
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    System.out.println(dateTime); // 2020-01-20T05:15
    dateTime = dateTime.minusDays(1);
    System.out.println(dateTime); // 2020-01-19T05:15
    dateTime = dateTime.minusHours(10);
    System.out.println(dateTime); // 2020-01-18T19:15
    dateTime = dateTime.minusSeconds(30);
    System.out.println(dateTime); // 2020-01-18T19:14:30
```

What methods can be called from which objects:

||Can call on LocalDate?|Can call on LocalTime?|Can call on LocalDateTime?|
|--|--|--|--|
|plusYears/minusYears|Yes|No|Yes|
|plusMonths/minusMonths|Yes|No|Yes|
|plusWeeks/minusWeeks|Yes|No|Yes|
|plusDays/minusDays|Yes|No|Yes|
|plusHours/minusHours|No|Yes|Yes|
|plusMinutes/minusMinutes|No|Yes|Yes|
|plusSeconds/minusSeconds|No|Yes|Yes|
|plusNanos/minusNanos|No|Yes|Yes|