Concatenation rules:

  * If both values are numeric, means addition.
  * If both values are string, means concatenation.
  * Expression is evaluated left to right.

``` java linenums="1 1 2"
    System.out.println(1 + 2); // 3
    System.out.println("a" + "b"); // ab
    System.out.println("a" + "b" + 3); // ab3
    System.out.println(1 + 2 + "c"); // 3c
```

Exam may try to trick you:

``` java linenums="1 1 2"
    int three = 3;
    String four = "4";
    System.out.println(1 + 2 + three + four);//"64"
```

