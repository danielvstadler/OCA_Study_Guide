Fields and instance initializer blocks are run in the order in which they appear in the file.

Constructor runs after all fields and iib.

``` java linenums="1 1 2"
    public class Chick { 
        private String name = "Fluffy"; 
        
        { System.out.println("setting field"); } 
    
        public Chick() {
            name = "Tiny"; 
            System.out.println("setting constructor"); 
        } 
    
        public static void main(String[] args) { 
            Chick chick = new Chick(); 
            System.out.println(chick.name); 
        } 
    
    } 

```

Running this example prints this: setting field > setting constructor > Tiny