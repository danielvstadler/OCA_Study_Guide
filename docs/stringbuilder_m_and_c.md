String methods create new strings and don't change current string.

StringBuilder methods don't.

``` java linenums="1 1 2"
    StringBuilder sb = new StringBuilder("start");
    sb.append("+middle"); // sb = "start+middle"
    StringBuilder same = sb.append("+end"); // "start+middle+end"

```

???+ important "Exam trick example"

    ``` java linenums="1 1 2"
        StringBuilder a = new StringBuilder("abc");
        StringBuilder b = a.append("de");
        b = b.append("f").append("g");
        System.out.println("a=" + a);
        System.out.println("b=" + b);
    ```

    In above example a and b = "abcdefg".

    Why? Because only one stringbuilder object is created(at line 1 with new). Reference b is just pointed to that object.

    Therefore methods called from b is happening to shared object.
