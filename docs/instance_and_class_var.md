##Instance Variables
Instance variables are also called fields. they are per instance object.

##Class Variables
Class variables are shared across all instances of the class. 

Class variables have the keyword `static` in its declaration.

!!! note "Instance and Class variables do not need to be initialized. They have default values."

| Variable type|Default init value|
|:-----|:-----:|
| boolean | false |
| byte, short, int, long | 0 |
| float, double | 0.0 |
| char | NUL |
| All other references | null |