arithmetic operators include:

  * addition(+)
  * subtraction(-)
  * multiplication(*)
  * division(/)
  * modulus(%)

Make use of parenthesis to change precedence.

