Different ways to create ArrayList:

``` java linenums="1 1 2"
    ArrayList list1 = new ArrayList();//create arraylist that uses defualt size
    ArrayList list2 = new ArrayList(10);//create arraylist with mutable size 10
    ArrayList list3 = new ArrayList(list2);//create arraylist with same size and content as arraylist list2
```

Java 5 introduced generics. To allow you to specify the type of class that the ArrayList will contain.

``` java linenums="1 1 2"
    ArrayList<String> list4 = new ArrayList<String>();
    ArrayList<String> list5 = new ArrayList<>();
```

ArrayList implements List. ArrayList is a List.

You can store an ArrayList in a List not vice versa.