```java linenums="1 1 2"
    public void eatMore(boolean hungry, int amountOfFood) {//parameter var hungry and amountOfFood can be used anywhere inside
        int roomInBelly = 5;//can be used in code blocks inside
        if (hungry) {
            boolean timeToEat = true;//can be used inside while loop
            while (amountOfFood > 0) {
                int amountEaten = 2;
                roomInBelly = roomInBelly - amountEaten; 
                amountOfFood = amountOfFood - amountEaten;
            }
        }
        System.out.println(amountOfFood);
    }
```

| variables | scope |
| -------- | ----- |
| Local |in scope for declaration to end of block|
| Instance |in scope from declaration until object garbage collected|
| Class |in scope from declaration until program ends|