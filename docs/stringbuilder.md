``` java linenums="1 1 2"
    String alpha = "";

    for(char current = 'a'; current <= 'z'; current++){
        alpha += current;
    }
    
    System.out.println(alpha);
```

String Object "" is created in line 1. Then a is appended, but strings are immutable therefor new string object "a" is created and "" is elligble for garbage collection. 

And so forth. A total of 27 objects are instantiated most of which are immediately eligible for garbage collection.

This is very inefficient. 

StringBuilder doesn't store interim strings and is mutable.

``` java linenums="1 1 2"
    StringBuilder alpha = new StringBuilder();
    for(char current = 'a'; current <= 'z'; current++){
        alpha.append(current);
    }

    System.out.println(alpha);
```

