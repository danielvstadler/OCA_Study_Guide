??? note "Converting to a Long"

    LocalDate and LocalDateTime have methods to convert them into long equvalents relative to 1970.

    1970 is what UNIX started using for date standards.

      * LocalDate has toEpochDay(), number of days to 1 Jan 1970.
      * LocalTime has toEpochTime(), number of seconds to 1970.

Example case of Period:

``` java linenums="1 1 2"
    public static void main(String[] args) {
        LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
        LocalDate end = LocalDate.of(2015, Month.MARCH, 30);
        Period period = Period.ofMonths(1); // create a period
        performAnimalEnrichment(start, end, period);
    }

    private static void performAnimalEnrichment(LocalDate start, LocalDate end,
    Period period) { // uses the generic period
        LocalDate upTo = start;
        while (upTo.isBefore(end)) {
            System.out.println("give new toy: " + upTo);
            upTo = upTo.plus(period); // adds the period
        }
    }
```

In the above example period is set to 1 month. which gets added to LocalDate Object.

Ways to create Period:

``` java linenums="1 1 2"
    Period annually = Period.ofYears(1); // every 1 year
    Period quarterly = Period.ofMonths(3); // every 3 months
    Period everyThreeWeeks = Period.ofWeeks(3); // every 3 weeks
    Period everyOtherDay = Period.ofDays(2); // every 2 days
    Period everyYearAndAWeek = Period.of(1, 0, 7); // every year and 7 days
```

!!! warning "Period methods can't be chained."

Periods can only be used with LocalDate and LocalDateTime, not LocalTime:

```java linenums="1 1 2"
    LocalDate date = LocalDate.of(2015, 1, 20);
    LocalTime time = LocalTime.of(6, 15);
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    Period period = Period.ofMonths(1);
    System.out.println(date.plus(period)); // 2015-02-20
    System.out.println(dateTime.plus(period)); // 2015-02-20T06:15
    System.out.println(time.plus(period)); // UnsupportedTemporalTypeException
```