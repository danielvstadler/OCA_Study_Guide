``` java linenums="1 1 2"
    char[] letters;
```

letters is a reference type not a primitive.

char is a primitive. char goes into array not the array type itself. array type is char[].

Array is an ordered list that can contain duplicates.