Garbage collection refers to the process of automatically freeing memory on the heap by deleting objects that are no longer reachable in your program.

There are different algorithms for garbage collection.

`System.gc()` meekly suggests to Java that now might be a good time to run garbage collection.

An object will remain on the heap untill it is no longer reachable. It is no longer reachable when:

  * No more references point to the object.
  * All references to the object have gone out of scope.

???+ note "Objects vs. References"
    A reference is a variable that has a name and can be used to access an object's content.
    References can be assigned to each other and all have the same size.

    An object sits on the heap and does not have a name. objects cannot be assigned to each other. Cannot access object without a reference. 
    It is the object that gets garbage collected, not its references.

Example:

``` java linenums="1 1 2"
    public class Scope {
        public static void main(String[] args) {
            String one, two;
            one = new String("a");//ref one = obj string("a")
            two = new String("b");//ref two = obj string("b")
            one = two;//ref one = ref two = obj string("b")
            //obj string("a") is garbage collected
            String three = one;//ref three= ref one = obj string("b")
            one = null;//ref one is null
            
        } 
    }
```
