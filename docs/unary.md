Unary operators require at least one operand, or variable to fuction.

|Unary operator|Description|
|------|--|
|+|Indicates a number is positive. numbers are assumed positive unless accompanied by - unary.|
|-|negation operator. Indicates literal number is negative.|
|++|Increment by 1. Pre-increment operator, returns new value. Post-increment operator returns current value, and then adds.|
|--|Decrement by 1. Pre-decrement operator, returns new value. Post-decrement operator returns current value, and then adds.|
|!|Logical complement operator. Invert boolean logical value|

