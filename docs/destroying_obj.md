Java does garbage collection automatically.

Java objects are stored in your program memory's heap. The heap or free store, represents a pool of unused memory allocated to your Java application. The heap size depends on your environment.

???+ note "Objects vs. References"
    