Numeric Promotion Rules:

  * If two values have different data types, Java promotes one of the values to the larges of the two data types.
  * If one value is int and the other is float, Java will automatically promote the int to float value data type.
  * `byte, short and char` are promoted to int any time they are used with java binary arithmatic operator.
  * After promotion and the operands have the same data type, the resulting value will be of new data type.

Examples:

``` java linenums="1 1 2"
    //x*y
    int x = 1;
    long y = 33;
    //Rule 1. long is bigger than int, so x will be promoted to long.

``` java linenums="1 1 2"
    //x+y?
    double x = 39.21;
    float y = 2.1;
    //Trick question. Code will not compile. floats without prefix f are assumed to be doubles.
``` 

``` java linenums="1 1 2"
    // x/y?
    short x = 10;
    short y = 3;
    // 
```

``` java linenums="1 1 2"
    // x * y / z?
    short x = 14;
    float y = 13;
    double z = 30;
    //All rules. x promoted to int. x promoted to float. x*y result promoted to double for z.
```