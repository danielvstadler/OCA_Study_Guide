A java operator is a special symbol that can be applied to a set of variables, values, or literals.
There are three flavors of operators in java:
  * unary
  * binary
  * ternary

???+ note "Order of operator precedence"
    |Operator|Symbols and examples|
    |-|-|
    |Post-unary operators|expression++, expression--|
    |Pre-unary operators|++expression, --expression|
    |Other unary operators|+, -, !|
    |Multiplication/Division/Modulus|*, /, %|
    |Addition/Subtraction|+, -|
    |Shift operators|<<, >>, >>>|
    |Relational operators|<, >, <=, >=, instanceof|
    |Equal to/not equal to|==, !=|
    |Logical operators|&, ^, \||
    |Short-circuit logical operators|&&, \|\||
    |Ternary operators|boolean expression ? expression1 : expression2|
    |Assignment operators|=, +=, -=, *=, /=, %=, &=, ^=, !=, <<=, >>=, >>>=|
