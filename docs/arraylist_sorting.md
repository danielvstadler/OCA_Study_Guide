Sorting an ArrayList is similar to sorting an Array. You just use a different helper class.

``` java linenums="1 1 2"
    List<Integer> numbers = new ArrayList<>();
    numbers.add(99);
    numbers.add(5);
    numbers.add(81);
    Collections.sort(numbers);
    System.out.println(numbers); [5, 81, 99]
```