``` java linenums="1 1 2"
    int[] numbers1 = new int[3];
```

numbers1 is a reference variable pointing to a Array object.

default value for int is 0. Therefore all elements in array have value 0.

Array index starts at 0 and goes up.

``` java linenums="1 1 2"
    int[] numbers2 = new int[] {42, 55, 99};
```

Another way to to create an array of size 3, but with giving those elements values.

``` java linenums="1 1 2"
    int[] numbers3 = {42, 55, 99}
```

Shorthand.

``` java linenums="1 1 2"
    int[] numAnimals;
    int [] numAnimals2;
    int numAnimals3[];
    int numAnimals4 [];
```

They all do the same and are correct.

``` java linenums="1 1 2"
    int[] ids, types;
    int ids[], types;
```

The first line creates two array variables.
The second line creates an array and an int.