String objects are not allowed to change. Immutable.

Cannot be made larger or smaller. Characters inside can't change.

This is the trade-off for optimal packing.