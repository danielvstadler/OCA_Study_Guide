Java classes are usually defined in their own *.java files, but more than one class can be defined per file.

IF you do this at most one class is allowed to be public, otherwise all public class not matching the name of the file will not compile.
