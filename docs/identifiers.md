Three rules to remember for legal identifiers:

  * Name must begin with a letter or the symbol $ and _.
  * Subsequent chars may also be numbers.
  * Cannot be the same as java reserved words.

???+ note "Conventions for identifier names"
    * Method and variables names begin with a lowercase letter followed by CamelCase. 
    * Class names begin with an uppercase letter followed by CamelCase.

???+ success "Legal Examples"
    * Okidentifier
    * $OK2Identifier
    * _alsoOK1d3ntifi3r
    * __SStillOkbutKnotsonice$

???+ danger "Illegal Examples"
    * 3DPointClass (Cannot begin with number)
    * hollywood@vine (@ is not allowed)
    * public (public is a reserved word)