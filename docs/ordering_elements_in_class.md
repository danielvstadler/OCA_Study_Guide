!!! note "remember PIC(Package, Imports, Class)"


|Element|Example|Required?|Where does it go?|
|--|--|:--:|--|
|Package declaration|package abc;|No|First line in file|
|import statements|import java.util.*|No|Immediately after the package|
|Class declaration|public class C|Yes|Immediately after the import|
|Field declaration|int i;|No|Anywhere inside a class|
|Method declaration|void method()|No|Anywhere inside a class|


Examples:

``` java linenums="1 1 2"
    package structure; // package must be first non-comment
    import java.util.*; // import must come after package
    public class Meerkat { // then comes the class
        double weight; // fields and methods can go in either order
        public double getWeight() {
            return weight; 
        }
        double height; // another field – they don't need to be together
    }
```

``` java linenums="1 1 2"
    /* header */
    package structure;
    // class Meerkat
    public class Meerkat { }
```

``` java linenums="1 1 2"
    import java.util.*;
    package structure; // DOES NOT COMPILE
    String name; // DOES NOT COMPILE
    public class Meerkat { }
```