##charAt(), indexOf(), length(), and substring()

``` java linenums="1 1 2"
    StringBuilder sb = new StringBuilder("animals");
    String sub = sb.substring(sb.indexOf("a"), sb.indexOf("al"));
    int len = sb.length();
    char ch = sb.charAt(6);
    System.out.println(sub + " " + len + " " + ch);//anim 7 s
```

!!! note "substring returns string, this is why sb doesn't change."

##append

``` java linenums="1 1 2"
    StringBuilder sb= new StringBuilder().append(1).append('c').append(true);
```

Don't need to convert different data types to string first.

##insert()

``` java linenums="1 1 2"
    StringBuilder sb = new StringBuilder("animals");
    sb.insert(7, "-"); // sb = animals 
    sb.insert(0, "-"); // sb = -animals 
    sb.insert(4, "-"); // sb = -ani-mals
    System.out.println(sb);
```

##delete() and deleteCharAt()

delete removes characters from sequence  and returns reference.

Includes last index.

``` java linenums="1 1 2"
    StringBuilder sb = new StringBuilder("abcdef");
    sb.delete(1, 3); // sb = adef
    sb.deleteCharAt(5); // throws an exception, sb is only 4 chars long now.
```

##reverse()

reversed characters in sequence and return reference.

``` java linenums="1 1 2"
    StringBuilder sb = new StringBuilder("ABC");
    sb.reverse();
    System.out.println(sb);
```

##toString()

converts StringBuilder to String. StringBuilder is used for performance purposes, but usually converted to string after prossecing is done.