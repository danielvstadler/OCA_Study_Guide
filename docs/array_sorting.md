##Sorting 

Using sort().

!!! note "Numbers sort before
letters and uppercase sorts before lowercase."

##Searching

Binary Search 

|Scenario|Result|
|:--|:--|
|Element Found. Sorted array.|index of match.|
|Element not Found. Sorted array.|Returns negative of index where number should go, -1.|
|Unsorted array|result is unpredictable.|

``` java linenums="1 1 2"
    int[] numbers = {2,4,6,8};
    System.out.println(Arrays.binarySearch(numbers, 2)); // 0
    System.out.println(Arrays.binarySearch(numbers, 4)); // 1
    System.out.println(Arrays.binarySearch(numbers, 1)); // -1
    System.out.println(Arrays.binarySearch(numbers, 3)); // -2
    System.out.println(Arrays.binarySearch(numbers, 9)); // -5
```

In line 4, 1 needs to be added to index -(0)-1.
In line 5, 3 needs to be added to index -(1)-1.
In line 6, 9 needs to be added to index -(4)-1.
