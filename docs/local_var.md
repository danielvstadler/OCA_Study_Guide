Local variables are defined inside methods.

!!! important "No default value, cannot use before initiliazation"


``` java linenums="1 1 2"
    public int notValid() {
        int y = 10;
        int x;
        int reply = x + y; // DOES NOT COMPILE
        return reply;
    } 
```

``` java linenums="1 1 2"
    public void findAnswer(boolean check) {
        int answer;
        int onlyOneBranch;
        
        if (check) {
            onlyOneBranch = 1;
            answer = 1;
        } else {
            answer = 2;
        }

        System.out.println(answer);
        System.out.println(onlyOneBranch); // DOES NOT COMPILE
}

```
