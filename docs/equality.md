``` java linenums="1 1 2"
    StringBuilder one = new StringBuilder();
    StringBuilder two = new StringBuilder();
    StringBuilder three = one.append("a");
    System.out.println(one == two); // false
    System.out.println(one == three); // true
```

This example isn't looking for primitives. It's evaluating whether references are pointing to the same object.

``` java linenums="1 1 2"
    String x = "Hello World";
    String y = "Hello World";
    System.out.println(x == y); // true
```

The JVM creates only one literal in memory(literals are pooled). x and y both point to that location in memory.

``` java linenums="1 1 2"
    //Example 1
    String x = "Hello World";
    String z = " Hello World".trim();
    System.out.println(x == z); // false

    //Example 2 
    String x = new String("Hello World");
    String y = "Hello World";
    System.out.println(x == y); // false

```

x is compile-time, meaning a literal. z is run-time, meaning a new stringobject is created. Example 1 and Example 2 are false for the same reason, literals can't be compared to String Objects.

``` java linenums="1 1 2"
    String x = "Hello World";
    String z = " Hello World".trim();
    System.out.println(x.equals(z)); // true
```

This is true because, equals checks the characters inside the string rather than the string itself.

== checks if references point to the same object.

!!! warning "Calling equals() on objects that don't implement equals() gives false."

!!! note "StringBuilder doesn't implement equal(), will compare references."