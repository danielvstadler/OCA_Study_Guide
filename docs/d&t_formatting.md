Methods to get date or time:

``` java linenums="1 1 2"
    LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
    System.out.println(date.getDayOfWeek()); // MONDAY
    System.out.println(date.getMonth()); // JANUARY
    System.out.println(date.getYear()); // 2020
    System.out.println(date.getDayOfYear()); // 20
```

Java provides class `DateTimeFormatter` that formats any date or time objects.

`DateTimeFormatter` comes in package `java.time.format`.

``` java linenums="1 1 2"
    LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
    LocalTime time = LocalTime.of(11, 12, 34);
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE));//2020-01-20
    System.out.println(time.format(DateTimeFormatter.ISO_LOCAL_TIME));//11:12:34
    System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));//2020-01-20T11:12:34
```

---

Example of `SHORT` formatting.

``` java linenums="1 1 2"
    DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    System.out.println(shortDateTime.format(dateTime)); // 1/20/20
    System.out.println(shortDateTime.format(date)); // 1/20/20
    System.out.println(shortDateTime.format(time)); // UnsupportedTemporalTypeException
```

!!! danger "Time cannot be formatted as date."

---

Example of `MEDUIM` formatting.

``` java linenums="1 1 2"
LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
LocalTime time = LocalTime.of(11, 12, 34);
LocalDateTime dateTime = LocalDateTime.of(date, time);
DateTimeFormatter shortF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
DateTimeFormatter mediumF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
System.out.println(shortF.format(dateTime)); // 1/20/20 11:12 AM
System.out.println(mediumF.format(dateTime)); // Jan 20, 2020 11:12:34 AM
```

---

Creating your own formats:

``` java linenums="1 1 2"
    DateTimeFormatter f = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm");
    System.out.println(dateTime.format(f)); // January 20, 2020, 11:12
```

__MMMM__ M outputs 1, MM outputs 01, MMM outputs Jan, and MMMM outputs January

__dd__ d outputs 1, dd outputs 01

__,__ output a comma

__yyyy__ y represents the year. yy outputs 2 digit year, yyyy outputs 4 digit year

__hh__ h represents the hour. hh includes leading 0.

__:__ output colon

__mm__ m represents the minute. mm includes leading 0.

