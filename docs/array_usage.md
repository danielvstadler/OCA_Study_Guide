``` java linenums="1 1 2"
    String[] mammals = {"monkey", "chimp", "donkey"};
    System.out.println(mammals.length); // 3
    System.out.println(mammals[0]); // monkey
    System.out.println(mammals[1]); // chimp
    System.out.println(mammals[2]); // donkey
```

``` java linenums="1 1 2"
    int[] numbers = new int[10];
    for (int i = 0; i < numbers.length; i++){
        numbers[i] = i + 5;
    }   
```