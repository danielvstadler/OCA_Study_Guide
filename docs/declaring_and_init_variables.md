Variables are declared the following way:
``` java linenums="1 1 2"
    String zooName;
    int numberAnimals;
    zooName = "The Best Zoo";
    numberAnimals = 100;

    //or 
    String zooName = "The Best Zoo"; 
    int numberAnimals = 100;
```
Declaring multiple variables:
``` java linenums="1 1 2"
    String s1, s2;
    boolean b1, b2; 
    String s3 = "yes", s4 = "no";
    int i1, i2, i3 = 0;//Declares 3 vars, inits 1

    String s1 = "1", s2;//Legal
    double d1, double d2;  //Illegal
    int i3; i4;  //-Illegal
```